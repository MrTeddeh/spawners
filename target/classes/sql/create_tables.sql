-- IMPORTANT NOTE: THIS FILE IS EXECUTED DIRECTLY AT PLUGIN START, MEANING ANY CHANGES TO THIS FILE GO LIVE
-- This is just used to create all the tables from scratch very helpful to keep all this consolidated into one
-- nicely formatted file

CREATE TABLE IF NOT EXISTS `user` (
    `uuid` BINARY(16) NOT NULL,
    `name` VARCHAR(16) NOT NULL,
    PRIMARY KEY (`uuid`)
);

CREATE TABLE IF NOT EXISTS `spawner_upgrade` (
    `type` INTEGER NOT NULL, -- ID of the entity
    `level` INTEGER NOT NULL,
    `data` VARCHAR(900) NOT NULL, -- This is so fucking massive because we need to store JSON keys
    `cost` DOUBLE NOT NULL,
    PRIMARY KEY (`type`, `level`)
);

CREATE TABLE IF NOT EXISTS `spawner` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `owner` BINARY(16) NOT NULL,
    `level` INTEGER NOT NULL,
    `count` INTEGER NOT NULL DEFAULT 1,
    `world` VARCHAR(20) NOT NULL,
    `location_x` INTEGER NOT NULL,
    `location_y` INTEGER NOT NULL,
    `location_z` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    FOREIGN KEY (`owner`) REFERENCES `user`(`uuid`) ON DELETE CASCADE,
    UNIQUE KEY `location`(`location_x`,`location_y`,`location_z`)
);
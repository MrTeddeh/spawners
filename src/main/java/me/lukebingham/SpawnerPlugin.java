package me.lukebingham;

import com.google.common.base.Preconditions;
import me.lukebingham.database.BaseDatabase;
import me.lukebingham.inventory.BaseMenuHandler;
import me.lukebingham.spawner.SpawnerManager;
import me.lukebingham.util.ThreadUtil;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.*;
import java.util.Scanner;

public class SpawnerPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        this.saveDefaultConfig();

        BaseDatabase.getInstance().init((YamlConfiguration) this.getConfig());
        this.testMySQL();

        new BaseMenuHandler();
        new SpawnerManager();
    }

    @Override
    public void onDisable() {

    }

    private void testMySQL() {
        Scanner scanner = new Scanner(SpawnerPlugin.class.getResourceAsStream("/sql/create_tables.sql"));
        scanner.useDelimiter("(;(\r)?\n)|(--\n)");

        try (Connection connection = BaseDatabase.getInstance().getConnection()) {
            Statement statement = connection.createStatement();

            while (scanner.hasNext()) {
                String line = scanner.next();
                if (line.startsWith("/*!") && line.endsWith("*/")) {
                    int index = line.indexOf(' ');
                    line = line.substring(index + 1, line.length() - " */".length());
                }

                if (line.trim().length() > 0) {
                    statement.execute(line);
                }
            }

            System.out.println("Database connection tested and working.");
            return;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        System.out.println("There was an issue with MySQL connecting.");
    }
}

package me.lukebingham.command;

import me.lukebingham.util.C;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;

import java.lang.reflect.Field;
import java.util.Arrays;

public abstract class CoreCommand<E extends CommandSender> extends BukkitCommand {

    private static CommandMap commandMap = null;

    private String noPermission = C.RED + "You don't have permission to use this!";
    private final String description;
    private String[] aliases;

    /**
     * Construct a new command.
     *
     * @param command command label
     * @param description command description
     * @param aliases command aliases
     */
    public CoreCommand(String command, String description, String... aliases) {
        super(command);
        this.description = description;
        this.aliases = aliases;

        register();
    }

    /**
     * Construct a new command.
     *
     * @param command command label
     * @param description command description
     */
    public CoreCommand(String command, String description) {
        super(command);
        this.description = description;

        register();
    }

    @Override
    public boolean execute(CommandSender commandSender, String s, String[] strings) {
        execute((E) commandSender, strings);
        return true;
    }

    /**
     * This method is fired when the command is executed.
     *
     * @param sender sender type of the command
     * @param args command arguments
     */
    public abstract void execute(E sender, String[] args);

    private void register() {
        if (commandMap != null) {
            if(this.aliases != null && this.aliases.length > 0)
                setAliases(Arrays.asList(this.aliases));
            setDescription(this.description);
            commandMap.register(super.getName(), this);
            return;
        }

        try {
            Field field = Bukkit.getServer().getClass().getDeclaredField("commandMap");
            field.setAccessible(true);
            commandMap = (CommandMap) field.get(Bukkit.getServer());

            if(this.aliases != null && this.aliases.length > 0)
                setAliases(Arrays.asList(this.aliases));
            setDescription(this.description);

            commandMap.register(super.getName(), this);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getCommand() {
        return super.getName();
    }

    @Override
    public String getDescription() {
        return description;
    }

    /**
     * This string is displayed the user<br>
     * when they don't have the required rank.
     *
     * @param input permission input
     */
    public void setNoPermissionMessage(String input) {
        this.noPermission = C.translate(input);
    }

    public void sendNoPermission(E sender) {
        sender.sendMessage(this.noPermission);
    }
}

package me.lukebingham.inventory.button;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

public interface IMenuClickAction {

    /**
     * This is fired when an item is interacted with.
     *
     * @param player    Player who interacted
     * @param clickType interaction type
     */
    void onClick(Player player, ClickType clickType);
}

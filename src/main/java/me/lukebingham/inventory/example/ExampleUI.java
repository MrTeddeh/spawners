package me.lukebingham.inventory.example;

import me.lukebingham.inventory.BaseMenu;
import me.lukebingham.inventory.button.ClickableItem;
import me.lukebingham.inventory.button.MenuItem;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ExampleUI extends BaseMenu {

    /**
     * Construct a new Menu.
     */
    public ExampleUI() {
        //Without MenuFlags
        super(6, "Example Interface");

        //With MenuFlags
        //super(6, "Example Interface", BaseMenuFlag.CLOSE_ON_NULL_CLICK, BaseMenuFlag.RESET_CURSOR_ON_OPEN);


        //new MenuItem(index, itemstack, allowPickup)
        addItem(new MenuItem(0, new ItemStack(Material.APPLE), false));

        //new ClickableItem(index, itemstack, menuClickAction)
        addItem(new ClickableItem(1, new ItemStack(Material.ENDER_PEARL, 4), (player, clickType) -> {
            player.sendMessage("You clicked the Ender pearl... nice! (" + clickType.name() + ")");
        }));

        //done.
    }

    //How to open this menu.
    //new ExampleUI().openInventory(player);
}

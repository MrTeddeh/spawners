package me.lukebingham.util;

public interface Callback<T> {
    void call(T arg);
}

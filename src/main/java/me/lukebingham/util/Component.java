package me.lukebingham.util;

import me.lukebingham.SpawnerPlugin;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public interface Component<T> extends Listener {

    /**
     * This will run when the component is loaded
     */
    default T onLoad() {
        return null;
    }

    /**
     * This will run when the component is unloaded
     */
    default T onDisable() {
        return null;
    }

    default void register() {
        Bukkit.getPluginManager().registerEvents(this, JavaPlugin.getPlugin(SpawnerPlugin.class));
    }
}

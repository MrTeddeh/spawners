package me.lukebingham.util;

import com.google.common.base.Preconditions;
import me.lukebingham.SpawnerPlugin;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.regex.Pattern;

public final class StringUtil {

    public static final String E_404 = "404";
    public static final String PREFIX;

    static {
        FileConfiguration configuration = JavaPlugin.getPlugin(SpawnerPlugin.class).getConfig();
        Preconditions.checkNotNull(configuration);

        PREFIX = get("prefix");
    }

    public static String get(String path, String... args) {
        FileConfiguration configuration = JavaPlugin.getPlugin(SpawnerPlugin.class).getConfig();
        Preconditions.checkNotNull(configuration);

        if (!configuration.contains(path))
            return E_404;

        String str = configuration.getString(path);

        if (str.contains("{prefix}") && !path.equals("prefix"))
            str = str.replace("{prefix}", PREFIX);

        for(int i = 0; i < args.length; i++) {
            if (!str.contains("{" + i + "}")) continue;
            str = str.replace("{" + i + "}", args[i]);
        }

        return C.translate(str);
    }

    public static String[] getArray(String path, String... args) {
        return get(path, args).split(Pattern.quote("{n}"));
    }
}

package me.lukebingham.util;

import me.lukebingham.SpawnerPlugin;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public final class ThreadUtil {

    public static void runSync(Runnable runnable) {
        Bukkit.getScheduler().runTask(JavaPlugin.getPlugin(SpawnerPlugin.class), runnable);
    }

    public static void runSyncLater(Runnable runnable, long ticks) {
        Bukkit.getScheduler().runTaskLater(JavaPlugin.getPlugin(SpawnerPlugin.class), runnable, ticks);
    }

    public static void runAsync(Runnable runnable) {
        Bukkit.getScheduler().runTaskAsynchronously(JavaPlugin.getPlugin(SpawnerPlugin.class), runnable);
    }

    public static void runAsyncLater(Runnable runnable, long ticks) {
        Bukkit.getScheduler().runTaskLaterAsynchronously(JavaPlugin.getPlugin(SpawnerPlugin.class), runnable, ticks);
    }
}

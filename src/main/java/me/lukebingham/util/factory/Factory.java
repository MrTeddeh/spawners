package me.lukebingham.util.factory;

public abstract class Factory<T> {
    protected T object;
    public abstract T build();
}

package me.lukebingham.util.factory;

public interface CloneableFactory<T> {
    T clone();
}

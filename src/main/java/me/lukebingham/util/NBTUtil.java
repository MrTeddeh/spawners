package me.lukebingham.util;

import net.minecraft.server.v1_8_R3.NBTBase;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagInt;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;

import java.util.Objects;

public final class NBTUtil {

    public static int getInteger(ItemStack is, String key) {

        // convert to NMS itemstack
        net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(is);

        // if tag system exists
        if (nmsStack.getTag() != null) {
            NBTTagCompound tag = nmsStack.getTag();

            try {
                return Integer.parseInt(tag.get(key).toString());
            } catch (Exception e) {
//                e.printStackTrace();
            }
        }

        return -1;
    }

    public static ItemStack setInteger(ItemStack is, String key, int value) {

        // convert to NMS itemstack
        net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(is);
        NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();
        Objects.requireNonNull(compound).set(key, new NBTTagInt(value));

        nmsStack.setTag(compound);
        return CraftItemStack.asBukkitCopy(nmsStack);
    }
}

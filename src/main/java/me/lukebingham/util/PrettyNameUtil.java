package me.lukebingham.util;

import com.google.common.collect.Maps;
import org.apache.commons.lang.WordUtils;
import org.bukkit.entity.EntityType;

import java.util.HashMap;

public final class PrettyNameUtil {

    private static final HashMap<EntityType, String> ENTITY_TYPE;

    static {
        ENTITY_TYPE = Maps.newHashMap();
        ENTITY_TYPE.put(EntityType.PIG_ZOMBIE, "ZOMBIE_PIGMAN");
    }

    public static String query(EntityType entityType) {
        return WordUtils.capitalize(ENTITY_TYPE.getOrDefault(entityType, entityType.getName()).replaceAll("_", " "));
    }
}

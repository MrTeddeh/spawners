package me.lukebingham.spawner;

import me.lukebingham.spawner.level.SpawnerLevel;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;

import java.util.UUID;

public interface Spawner {

    /**
     * Get the unique identifier of the spawner.
     *
     * @return unique identifier
     */
    int getIdentifier();

    /**
     * Get the UUID owner that placed this spawner.
     *
     * @return UUID of the owner
     */
    UUID getOwner();

    /**
     * Get the name of the owner that placed this spawner.
     *
     * @return Name of the owner
     */
    String getOwnerName();

    /**
     * Get the {@link SpawnerLevel} data object for this spawner.
     *
     * @param type - Entity type of the spawner
     * @return {@link SpawnerLevel} data object
     */
    SpawnerLevel getSpawnerLevel(EntityType type);

    /**
     * Set the level of the spawner.
     *
     * @param level - new level to set
     */
    void setSpawnerLevel(int level);

    /**
     * Get the location of the spawner.
     *
     * @return location
     */
    Location getLocation();

    /**
     * Get the amount of stacked spawners.
     *
     * @return combined stack
     */
    int getCount();
    void setCount(int value);
}

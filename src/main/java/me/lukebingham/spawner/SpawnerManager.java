package me.lukebingham.spawner;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import me.lukebingham.database.BaseDatabase;
import me.lukebingham.spawner.command.SpawnerGiveCommand;
import me.lukebingham.spawner.dao.SpawnerDAO;
import me.lukebingham.spawner.level.SpawnerLevel;
import me.lukebingham.util.NBTUtil;
import me.lukebingham.util.PrettyNameUtil;
import me.lukebingham.util.StringUtil;
import me.lukebingham.util.ThreadUtil;
import me.lukebingham.util.factory.ItemFactory;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public final class SpawnerManager {

    final Map<EntityType, SpawnerLevel[]> spawnerLevelMap;
    private final HashMap<Location, Spawner> spawnerMap;

    public SpawnerManager() {
        this.spawnerLevelMap = Maps.newHashMap();
        loadSpawnerLevels();

        this.spawnerMap = Maps.newHashMap();
        ThreadUtil.runAsync(() -> {
            SpawnerDAO.getAllSpawners(this).forEach(spawner -> this.spawnerMap.put(spawner.getLocation(), spawner));
        });

        new SpawnerHandler(this);
        new SpawnerGiveCommand(this);
    }

    public Spawner getSpawnerByLoc(Location location) {
        return this.spawnerMap.getOrDefault(location, null);
    }

    public void addSpawner(Spawner spawner) {
        this.spawnerMap.put(spawner.getLocation(), spawner);
    }

    public void destructSpawner(Spawner spawner, boolean runQuery) {
        this.spawnerMap.remove(spawner.getLocation());

        if (!runQuery) return;

        ThreadUtil.runAsync(() -> SpawnerDAO.deleteSpanwer(spawner.getIdentifier()));
    }

    public SpawnerLevel[] getSpawnerLevels(EntityType type) {
        return this.spawnerLevelMap.getOrDefault(type, null);
    }

    public void registerLevel(EntityType type, SpawnerLevel spawnerLevel) {
        SpawnerLevel[] array = spawnerLevelMap.getOrDefault(type, new SpawnerLevel[3]);
        array[spawnerLevel.getLevel() - 1] = spawnerLevel;
        spawnerLevelMap.put(type, array);
    }

    private void loadSpawnerLevels() {
        ThreadUtil.runAsync(() -> {
            SpawnerDAO.getSpawnerLevels(this);

            // If there isn't any entries in the database, let's add some.
            if (this.spawnerLevelMap.isEmpty()) {
                File levelsDir = new File(Bukkit.getWorldContainer(), ".spawners");
                if (!levelsDir.exists()) return;

                try (Connection connection = BaseDatabase.getInstance().getConnection()) {
                    Preconditions.checkNotNull(connection);

                    for (File entityDir : Objects.requireNonNull(levelsDir.listFiles())) {
                        int id = Integer.parseInt(entityDir.getName());
                        EntityType type = EntityType.fromId(id);
                        Preconditions.checkNotNull(type);

                        System.out.println("Found entity file for " + type.name());

                        JSONParser parser = new JSONParser();
                        for (File jsonFile : Objects.requireNonNull(entityDir.listFiles())) {
                            int level = Integer.parseInt(jsonFile.getName().replace(".json", ""));

                            try (FileReader reader = new FileReader(jsonFile)) {
                                Object object = parser.parse(reader);
                                JSONArray json = (JSONArray) object;

                                try (PreparedStatement statement = connection.prepareStatement("INSERT INTO `spawner_upgrade` (type, level, data, cost) VALUES (?,?,?,?);")) {
                                    statement.setInt(1, id);
                                    statement.setInt(2, level);
                                    statement.setString(3, json.toString());
                                    statement.setDouble(4, (level * 100)); // 100, 200, 300

                                    statement.execute();
                                    System.out.println("Inserted " + type.name() + " level " + level);
                                }

                            } catch (IOException | ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }

                loadSpawnerLevels();
            }
        });
    }

    public ItemStack constructSpawnerItem(Player player, int count, int level, EntityType type) {
        ItemStack itemStack = new ItemFactory(Material.MOB_SPAWNER, (byte) type.getTypeId())
                .setAmount(count)
                .setName(StringUtil.get("item.name", PrettyNameUtil.query(type), Integer.toString(level)))
                .setLore(StringUtil.getArray("item.lore", PrettyNameUtil.query(type), Integer.toString(level), player.getName()))
                .build();

        itemStack = NBTUtil.setInteger(itemStack, SpawnerHandler.LEVEL, level);
        itemStack = NBTUtil.setInteger(itemStack, SpawnerHandler.TYPE, type.getTypeId());
        return itemStack;
    }
}

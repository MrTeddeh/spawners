package me.lukebingham.spawner;

import com.google.common.base.Preconditions;
import me.lukebingham.database.BaseDatabase;
import me.lukebingham.spawner.dao.SpawnerDAO;
import me.lukebingham.spawner.level.SpawnerLevel;
import me.lukebingham.util.ThreadUtil;
import me.lukebingham.util.UUIDUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.UUID;

public final class BaseSpawner implements Spawner {

    private final SpawnerManager spawnerManager;

    private int identifier = -1;
    private UUID owner;
    private String ownerName;
    private Location location = null;

    private int level;
    private int count = 1;

    /**
     * Construct a new Spawner
     */
    public BaseSpawner(SpawnerManager spawnerManager, int identifier, UUID owner, String ownerName, Location location, int level) {
        this.spawnerManager = spawnerManager;
        this.identifier = identifier;
        this.owner = owner;
        this.ownerName = ownerName;
        this.location = location;
        this.level = level;
    }

    /**
     * Construct a new Spawner
     */
    public BaseSpawner(SpawnerManager spawnerManager, int identifier, UUID owner, String ownerName, Location location, int level, int count) {
        this(spawnerManager, identifier, owner, ownerName, location, level);
        this.count = count;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getIdentifier() {
        return this.identifier;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UUID getOwner() {
        return this.owner;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getOwnerName() {
        return this.ownerName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SpawnerLevel getSpawnerLevel(EntityType type) {
        SpawnerLevel[] levels = this.spawnerManager.spawnerLevelMap.getOrDefault(type, null);

        // Make sure the levels array isn't null.
        Preconditions.checkNotNull(levels);

        // Make sure the levels length is equal to or larger than our current level.
        Preconditions.checkState(levels.length >= this.level);

        return levels[this.level - 1];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSpawnerLevel(int value) {
        this.level = value;
        ThreadUtil.runAsync(() -> SpawnerDAO.setLevel(this.identifier, value));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Location getLocation() {
        return this.location;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getCount() {
        return count;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setCount(int value) {
        this.count = value;
        ThreadUtil.runAsync(() -> SpawnerDAO.setCount(this.identifier, value));
    }
}
package me.lukebingham.spawner;

import com.google.common.base.Preconditions;
import me.lukebingham.database.BaseDatabase;
import me.lukebingham.spawner.dao.SpawnerDAO;
import me.lukebingham.spawner.inventory.SpawnerMenu;
import me.lukebingham.util.*;
import me.lukebingham.util.factory.FireworkFactory;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.TileEntityMobSpawner;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.craftbukkit.v1_8_R3.block.CraftCreatureSpawner;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.SpawnerSpawnEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public final class SpawnerHandler implements Component<SpawnerManager> {

    protected static final String LEVEL = "base_level";
    protected static final String TYPE = "base_type";

    private final SpawnerManager spawnerManager;

    public SpawnerHandler(SpawnerManager spawnerManager) {
        this.spawnerManager = spawnerManager;

        // Register the event.
        this.register();
    }

    @EventHandler // TODO, Note this is just to popularise the `user` table.
    protected final void onPlayerJoin(AsyncPlayerPreLoginEvent event) {
        try (Connection connection = BaseDatabase.getInstance().getConnection()) {
            Preconditions.checkNotNull(connection);

            try (PreparedStatement statement = connection.prepareStatement("INSERT INTO `user` (`uuid`,`name`) VALUES (UNHEX(?),?) ON DUPLICATE KEY UPDATE `name`=?;")) {
                statement.setString(1, event.getUniqueId().toString().replace("-", ""));
                statement.setString(2, event.getName());
                statement.setString(3, event.getName());

                statement.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @EventHandler(ignoreCancelled = true)
    protected final void onSpawnerPlace(BlockPlaceEvent event) {
        if (event.getBlock() == null)
            return;

        if (event.getBlock().getType() != Material.MOB_SPAWNER)
            return;

        ItemStack itemStack = event.getItemInHand();
        Preconditions.checkNotNull(itemStack);

        int level = NBTUtil.getInteger(itemStack, LEVEL);
        int type = NBTUtil.getInteger(itemStack, TYPE);

        if (level == -1) level = 1;
        if (type == -1) type = 90; //Default to pig.

        CreatureSpawner creatureSpawner = (CreatureSpawner) event.getBlock().getState();
        creatureSpawner.setSpawnedType(EntityType.fromId(type));
        creatureSpawner.update(true);

        CreatureSpawner relative = this.combineSpawner(creatureSpawner, level);

        if (relative != null) {
            //Cancel the event due to TileEntity errors.
            event.setCancelled(true);

            //Reduce spawners in hand.
            if (event.getItemInHand().getAmount() <= 1) {
                event.getPlayer().setItemInHand(new ItemStack(Material.AIR));
            } else {
                event.getItemInHand().setAmount(event.getItemInHand().getAmount() - 1);
            }

            Firework firework = new FireworkFactory(relative.getLocation().clone().add(0.5, 0.5, 0.5)).setColor(Color.GREEN).setType(FireworkEffect.Type.BALL).setTrail(true).setPower(0).build();
            ThreadUtil.runSyncLater(firework::detonate, 3);
            return;
        }

        SpawnerDAO.createSpawner(this.spawnerManager, event.getPlayer(), creatureSpawner.getLocation(), level, arg -> {
            if (!arg.isPresent()) {
                event.setCancelled(true);
                event.getPlayer().sendMessage(C.RED + "Something went wrong, try again!");
                return;
            }

            this.spawnerManager.addSpawner(arg.get());
        });
    }

    @EventHandler(ignoreCancelled = true)
    protected final void onSpawnerInteract(PlayerInteractEvent event) {
        if (event.getAction() != Action.RIGHT_CLICK_BLOCK)
            return;

        if (event.getClickedBlock() == null || event.getClickedBlock().getType() != Material.MOB_SPAWNER)
            return;

        if (event.getPlayer().isSneaking())
            return;

        CreatureSpawner creatureSpawner = (CreatureSpawner) event.getClickedBlock().getState();
        Spawner spawner = this.spawnerManager.getSpawnerByLoc(creatureSpawner.getLocation());
        if (spawner == null) return;

        if (!event.getPlayer().getUniqueId().equals(spawner.getOwner())) {
            event.getPlayer().sendMessage(StringUtil.get("notOwned", spawner.getOwnerName()));
            return;
        }

        new SpawnerMenu(this.spawnerManager, spawner, creatureSpawner.getSpawnedType()).openInventory(event.getPlayer());
    }

    @EventHandler(ignoreCancelled = true)
    protected final void onSpawnerBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();

        if (event.getBlock() == null || event.getBlock().getType() != Material.MOB_SPAWNER)
            return;

        CreatureSpawner creatureSpawner = (CreatureSpawner) event.getBlock().getState();
        EntityType type = creatureSpawner.getSpawnedType();

        Spawner spawner = this.spawnerManager.getSpawnerByLoc(creatureSpawner.getLocation());

        // This must be an old spawner, remove it automatically.
        if (spawner == null) {
            if (!this.canCollectSpawner(player))
                return;

            ItemStack drop = this.spawnerManager.constructSpawnerItem(player, 1, 1, type);
            player.getInventory().addItem(drop);
            player.updateInventory();
            return;
        }

        if (player.isSneaking()) {
            if (this.canCollectSpawner(player)) {
                ItemStack drop = this.spawnerManager.constructSpawnerItem(player, spawner.getCount(), spawner.getSpawnerLevel(type).getLevel(), type);
                player.getInventory().addItem(drop);
                player.updateInventory();
            }

            event.setCancelled(true);
            event.getBlock().setType(Material.AIR);
            this.spawnerManager.destructSpawner(spawner, true);
            return;
        }

        event.setCancelled(true);

        if (this.canCollectSpawner(player)) {
            ItemStack drop = this.spawnerManager.constructSpawnerItem(player, 1, spawner.getSpawnerLevel(type).getLevel(), type);
            player.getInventory().addItem(drop);
            player.updateInventory();
        }

        spawner.setCount(spawner.getCount() - 1);
        if (spawner.getCount() > 0)
            return;

        event.getBlock().setType(Material.AIR);
        this.spawnerManager.destructSpawner(spawner, true);
    }

    private CreatureSpawner combineSpawner(CreatureSpawner creatureSpawner, int level) {
        for(BlockFace face : new BlockFace[] {BlockFace.UP, BlockFace.DOWN, BlockFace.NORTH, BlockFace.EAST, BlockFace.SOUTH, BlockFace.WEST}) {
            Block block = creatureSpawner.getBlock().getRelative(face);

            if (block == null || block.getType() != Material.MOB_SPAWNER)
                continue;

            CreatureSpawner relative = (CreatureSpawner) block.getState();
            Spawner relativeSpawner = this.spawnerManager.getSpawnerByLoc(relative.getLocation());
            if (relativeSpawner == null) continue;

            if (relative.getSpawnedType() != creatureSpawner.getSpawnedType())
                continue;

            if (relativeSpawner.getSpawnerLevel(creatureSpawner.getSpawnedType()).getLevel() != level)
                continue;

            relativeSpawner.setCount(relativeSpawner.getCount() + 1);
            return relative;
        }

        return null;
    }

    private boolean canCollectSpawner(Player player) {
        return player.getItemInHand().getEnchantments().containsKey(Enchantment.SILK_TOUCH) && (player.hasPermission("skytropia.spawner.silk_touch") || player.isOp());
    }
}

package me.lukebingham.spawner.inventory;

import com.google.common.base.Preconditions;
import me.lukebingham.inventory.BaseMenu;
import me.lukebingham.inventory.BaseMenuFlag;
import me.lukebingham.inventory.button.ClickableItem;
import me.lukebingham.inventory.button.MenuItem;
import me.lukebingham.spawner.Spawner;
import me.lukebingham.spawner.SpawnerManager;
import me.lukebingham.spawner.level.SpawnerLevel;
import me.lukebingham.util.C;
import me.lukebingham.util.StringUtil;
import me.lukebingham.util.ThreadUtil;
import me.lukebingham.util.factory.FireworkFactory;
import me.lukebingham.util.factory.ItemFactory;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.ItemStack;

public final class SpawnerMenu extends BaseMenu {

    private static final int[] NULL_SLOTS = {0,1,2,3,4,5,6,7,8, 9,10,12,14,16,17, 18,19,20,21,22,23,24,25,26};
    private static final int[] LEVEL_SLOTS = {11,13,15};

    public SpawnerMenu(SpawnerManager spawnerManager, Spawner spawner, EntityType type) {
        super(
                3,
                StringUtil.get("upgrade.menu",
                        Integer.toString(spawner.getSpawnerLevel(type).getLevel()),
                        "404",
                        Integer.toString(spawner.getCount())
                ),
                BaseMenuFlag.CLOSE_ON_NULL_CLICK
        );

        for (int i : NULL_SLOTS) {
            ItemStack stack = new ItemFactory(Material.STAINED_GLASS_PANE, (byte)14).setName("").build();
            super.addItem(new MenuItem(i, stack, false));
        }

        ItemFactory iron = new ItemFactory(Material.IRON_BLOCK);
        ItemFactory gold = new ItemFactory(Material.GOLD_BLOCK);
        ItemFactory diamond = new ItemFactory(Material.DIAMOND_BLOCK);

        SpawnerLevel[] levels = spawnerManager.getSpawnerLevels(type);
        Preconditions.checkNotNull(levels);

        int level = spawner.getSpawnerLevel(type).getLevel();
        int count = spawner.getCount();

        switch (level) {
            case 1:
                iron.setName(StringUtil.get("upgrade.selected.name", "1", Double.toString(levels[0].getCost() * spawner.getCount()), Integer.toString(count))).setLore(StringUtil.getArray("upgrade.selected.lore", "1", Double.toString(levels[0].getCost() * spawner.getCount()), Integer.toString(count))).setGlowing(true);
                gold.setName(StringUtil.get("upgrade.purchase.name", "2", Double.toString(levels[1].getCost() * spawner.getCount()), Integer.toString(count))).setLore(StringUtil.getArray("upgrade.purchase.lore", "2", Double.toString(levels[1].getCost() * spawner.getCount()), Integer.toString(count)));
                diamond.setName(StringUtil.get("upgrade.purchase.name", "3", Double.toString(levels[2].getCost() * spawner.getCount()), Integer.toString(count))).setLore(StringUtil.getArray("upgrade.purchase.lore", "3", Double.toString(levels[2].getCost() * spawner.getCount()), Integer.toString(count)));

                super.addItem(new MenuItem(LEVEL_SLOTS[0], iron.build(), false));

                //Clickable item
                super.addItem(new ClickableItem(LEVEL_SLOTS[1], gold.build(), (player, clickType) -> {
                    //TODO - Hook into an economy system.

                    spawner.setSpawnerLevel(2);
                    Firework firework = new FireworkFactory(spawner.getLocation().clone().add(0.5, 0.5, 0.5)).setColor(Color.GREEN).setType(FireworkEffect.Type.BALL).setTrail(true).setPower(0).build();
                    ThreadUtil.runSyncLater(firework::detonate, 3);
                    player.closeInventory();
                }));

                //Clickable item
                super.addItem(new ClickableItem(LEVEL_SLOTS[2], diamond.build(), (player, clickType) -> {
                    //TODO - Hook into an economy system.

                    spawner.setSpawnerLevel(3);
                    Firework firework = new FireworkFactory(spawner.getLocation().clone().add(0.5, 0.5, 0.5)).setColor(Color.GREEN).setType(FireworkEffect.Type.BALL).setTrail(true).setPower(0).build();
                    ThreadUtil.runSyncLater(firework::detonate, 3);
                    player.closeInventory();
                }));
                break;

            case 2:
                iron.setName(StringUtil.get("upgrade.owned.name", "1", Double.toString(levels[0].getCost() * spawner.getCount()), Integer.toString(count))).setLore(StringUtil.getArray("upgrade.owned.lore", "1", Double.toString(levels[0].getCost() * spawner.getCount()), Integer.toString(count)));
                gold.setName(StringUtil.get("upgrade.selected.name", "2", Double.toString(levels[1].getCost() * spawner.getCount()), Integer.toString(count))).setLore(StringUtil.getArray("upgrade.selected.lore", "2", Double.toString(levels[1].getCost() * spawner.getCount()), Integer.toString(count))).setGlowing(true);
                diamond.setName(StringUtil.get("upgrade.purchase.name", "3", Double.toString(levels[2].getCost() * spawner.getCount()), Integer.toString(count))).setLore(StringUtil.getArray("upgrade.purchase.lore", "3", Double.toString(levels[2].getCost() * spawner.getCount()), Integer.toString(count)));

                super.addItem(new MenuItem(LEVEL_SLOTS[0], iron.build(), false));
                super.addItem(new MenuItem(LEVEL_SLOTS[1], gold.build(), false));

                //Clickable item
                super.addItem(new ClickableItem(LEVEL_SLOTS[2], diamond.build(), (player, clickType) -> {
                    //TODO - Hook into an economy system.

                    spawner.setSpawnerLevel(3);
                    Firework firework = new FireworkFactory(spawner.getLocation().clone().add(0.5, 0.5, 0.5)).setColor(Color.GREEN).setType(FireworkEffect.Type.BALL).setTrail(true).setPower(0).build();
                    ThreadUtil.runSyncLater(firework::detonate, 3);
                    player.closeInventory();
                }));
                break;

            case 3:
                iron.setName(StringUtil.get("upgrade.owned.name", "1", Double.toString(levels[0].getCost() * spawner.getCount()), Integer.toString(count))).setLore(StringUtil.getArray("upgrade.owned.lore", "1", Double.toString(levels[0].getCost() * spawner.getCount()), Integer.toString(count)));
                gold.setName(StringUtil.get("upgrade.owned.name", "2", Double.toString(levels[1].getCost() * spawner.getCount()), Integer.toString(count))).setLore(StringUtil.getArray("upgrade.owned.lore", "2", Double.toString(levels[1].getCost() * spawner.getCount()), Integer.toString(count)));
                diamond.setName(StringUtil.get("upgrade.selected.name", "3", Double.toString(levels[2].getCost() * spawner.getCount()), Integer.toString(count))).setLore(StringUtil.getArray("upgrade.selected.lore", "3", Double.toString(levels[2].getCost() * spawner.getCount()), Integer.toString(count))).setGlowing(true);

                super.addItem(new MenuItem(LEVEL_SLOTS[0], iron.build(), false));
                super.addItem(new MenuItem(LEVEL_SLOTS[1], gold.build(), false));
                super.addItem(new MenuItem(LEVEL_SLOTS[2], diamond.build(), false));
                break;

            default:
                break;
        }
    }
}

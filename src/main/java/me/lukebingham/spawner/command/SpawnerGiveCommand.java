package me.lukebingham.spawner.command;

import me.lukebingham.command.CoreCommand;
import me.lukebingham.spawner.SpawnerManager;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

public final class SpawnerGiveCommand extends CoreCommand<Player> {

    private final SpawnerManager spawnerManager;

    public SpawnerGiveCommand(SpawnerManager spawnerManager) {
        super("skyspawner", "Test command for giving a spawner.");
        this.spawnerManager = spawnerManager;
    }

    @Override
    public void execute(Player sender, String[] args) {
        if (args.length > 0) {
            EntityType type = EntityType.valueOf(args[0]);
            sender.getInventory().addItem(this.spawnerManager.constructSpawnerItem(sender, 1, 1, type));
            sender.updateInventory();
        }
    }
}

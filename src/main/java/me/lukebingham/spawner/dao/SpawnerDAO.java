package me.lukebingham.spawner.dao;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import me.lukebingham.database.BaseDatabase;
import me.lukebingham.spawner.BaseSpawner;
import me.lukebingham.spawner.Spawner;
import me.lukebingham.spawner.SpawnerManager;
import me.lukebingham.spawner.level.BaseSpawnerLevel;
import me.lukebingham.spawner.level.SpawnerLevel;
import me.lukebingham.spawner.level.SpawnerLevelData;
import me.lukebingham.util.Callback;
import me.lukebingham.util.ThreadUtil;
import me.lukebingham.util.UUIDUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.sql.*;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

public final class SpawnerDAO {

    public static void createSpawner(SpawnerManager spawnerManager, Player owner, Location location, int level, Callback<Optional<Spawner>> callback) {
        Preconditions.checkNotNull(spawnerManager);

        ThreadUtil.runAsync(() -> {
            try (Connection connection = BaseDatabase.getInstance().getConnection()) {
                Preconditions.checkNotNull(connection);

                try (PreparedStatement statement = connection.prepareStatement("INSERT INTO spawner (owner, level, world, location_x, location_y, location_z) VALUES (UNHEX(?),?,?,?,?,?);", Statement.RETURN_GENERATED_KEYS)) {
                    statement.setString(1, owner.getUniqueId().toString().replaceAll("-", ""));
                    statement.setInt(2, level);
                    statement.setString(3, location.getWorld().getName());
                    statement.setInt(4, location.getBlockX());
                    statement.setInt(5, location.getBlockY());
                    statement.setInt(6, location.getBlockZ());

                    statement.executeUpdate();
                    try (ResultSet result = statement.getGeneratedKeys()) {
                        if (result.next()) {

                            callback.call(Optional.of(new BaseSpawner(
                                    spawnerManager,
                                    result.getInt(1),
                                    owner.getUniqueId(),
                                    owner.getName(),
                                    location,
                                    level
                            )));

                            return;
                        }
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }

            callback.call(Optional.empty());
        });
    }

    public static void setCount(int identifier, int value) {
        try (Connection connection = BaseDatabase.getInstance().getConnection()) {
            Preconditions.checkNotNull(connection);

            try (PreparedStatement statement = connection.prepareStatement("UPDATE `spawner` SET `count`=? WHERE `id`=?;")) {
                statement.setInt(1, value);
                statement.setInt(2, identifier);

                statement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void setLevel(int identifier, int value) {
        try (Connection connection = BaseDatabase.getInstance().getConnection()) {
            Preconditions.checkNotNull(connection);

            try (PreparedStatement statement = connection.prepareStatement("UPDATE `spawner` SET `level`=? WHERE `id`=?;")) {
                statement.setInt(1, value);
                statement.setInt(2, identifier);

                statement.executeUpdate();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void deleteSpanwer(int identifier) {
        try (Connection connection = BaseDatabase.getInstance().getConnection()) {
            Preconditions.checkNotNull(connection);

            try (PreparedStatement statement = connection.prepareStatement("DELETE FROM `spawner` WHERE `id`=?;")) {
                statement.setInt(1, identifier);

                statement.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void getSpawnerLevels(SpawnerManager spawnerManager) {
        Preconditions.checkNotNull(spawnerManager);

        try (Connection connection = BaseDatabase.getInstance().getConnection()) {
            Preconditions.checkNotNull(connection);

            try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM `spawner_upgrade` WHERE 1;")) {
                try (ResultSet result = statement.executeQuery()) {
                    Gson gson = new Gson();

                    while (result.next()) {
                        int type_id = result.getInt("type");
                        EntityType type = EntityType.fromId(type_id);

                        SpawnerLevelData[] data = gson.fromJson(result.getString("data"), SpawnerLevelData[].class);
                        Preconditions.checkNotNull(data);

                        SpawnerLevel level = new BaseSpawnerLevel(
                                result.getInt("level"),
                                data,
                                result.getDouble("cost")
                        );

                        spawnerManager.registerLevel(type, level);
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static Set<Spawner> getAllSpawners(SpawnerManager spawnerManager) {
        Set<Spawner> set = Sets.newHashSet();

        try (Connection connection = BaseDatabase.getInstance().getConnection()) {
            Preconditions.checkNotNull(connection);

            try (PreparedStatement statement = connection.prepareStatement("SELECT S.id, HEX(S.owner) AS uid, S.level, S.count, S.world, S.location_x AS x, S.location_y AS y, S.location_z AS z, U.name FROM `spawner` S, `user` U WHERE 1;")) {

                try (ResultSet result = statement.executeQuery()) {
                    while (result.next()) {
                        UUID uuid = UUIDUtil.createUUID(result.getString("uid")).orElse(null);
                        if (uuid == null) continue;

                        World world = Bukkit.getWorld(result.getString("world"));
                        if (world == null) continue;

                        Location location = new Location(
                                world,
                                result.getInt("x"),
                                result.getInt("y"),
                                result.getInt("z")
                        );

                        set.add(new BaseSpawner(
                                spawnerManager,
                                result.getInt("id"),
                                uuid,
                                result.getString("name"),
                                location,
                                result.getInt("level"),
                                result.getInt("count")
                        ));
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return set;
    }
}

package me.lukebingham.spawner.level;

public final class BaseSpawnerLevel implements SpawnerLevel {

    private final int level;
    private final SpawnerLevelData[] data;
    private final double cost;

    /**
     * Construct a new Spawner level data object.
     */
    public BaseSpawnerLevel(int level, SpawnerLevelData[] data, double cost) {
        this.level = level;
        this.data = data;
        this.cost = cost;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getLevel() {
        return this.level;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SpawnerLevelData[] getLevelData() {
        return this.data;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public double getCost() {
        return this.cost;
    }
}

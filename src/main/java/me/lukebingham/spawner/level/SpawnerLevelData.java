package me.lukebingham.spawner.level;

public final class SpawnerLevelData {

    private String amount;
    private String chance;
    private String type;
    private String material;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getChance() {
        return chance;
    }

    public void setChance(String chance) {
        this.chance = chance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    @Override
    public String toString() {
        return "Data [amount = " + amount + ", chance = " + chance + ", type = " + type + ", material = " + material + "]";
    }
}

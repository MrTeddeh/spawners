package me.lukebingham.spawner.level;

public interface SpawnerLevel {

    /**
     * Get the level assigned to this data interface.
     *
     * @return assigned level
     */
    int getLevel();

    /**
     * Get the {@link SpawnerLevelData} assigned
     * to this data interface.
     *
     * @return assigned {@link SpawnerLevelData}
     */
    SpawnerLevelData[] getLevelData();

    /**
     * Get the cost of this data interface.
     *
     * @return assigned cost
     */
    double getCost();
}
